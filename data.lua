energy_per_explosive = 2.4 --MJ 

require("prototypes.fuel-category")
require("prototypes.entities.entities")
require("prototypes.items.items")
require("prototypes.recipes.recipes")
require("prototypes.technology.technology")

data.raw["item"]["explosives"].fuel_value = energy_per_explosive.."MJ"
data.raw["item"]["explosives"].fuel_category = "explosive"