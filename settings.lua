data:extend({
	-- runtime
	{
		type = "bool-setting",
		name = "explosive-mining-enable-tiers",
		order = "a",
		setting_type = "startup",
		default_value = true,
	}
})
