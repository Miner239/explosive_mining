function create_item(tier)
    local newitem = table.deepcopy(data.raw["item"]["electric-mining-drill"])

    newitem.name = "explosive-mining-drill-"..tier
    newitem.order = "a[items]-a[explosive-mining-drill-"..tier.."]"
    newitem.place_result = "explosive-mining-drill-"..tier

    data:extend({newitem})
end

create_item(1)

if settings.startup["explosive-mining-enable-tiers"].value == true then
    for i=2,4 do
        create_item(i)
    end
end