data:extend(
    {
        {
            type = "technology",
            name = "explosive-mining-1",
            icon_size = 128,
            icon = "__base__/graphics/technology/mining-productivity.png",
            effects =
            {
              {
                type = "unlock-recipe",
                recipe = "explosive-mining-drill-1"
              }
            },
            prerequisites = {"explosives"},
            unit =
            {
              count = 250,
              ingredients = data.raw.technology.explosives.unit.ingredients,
              time = 30
            },
            order = "c-k-f"
        }
    }
)

if settings.startup["explosive-mining-enable-tiers"].value == true then
    data:extend(
        {
            {
                type = "technology",
                name = "explosive-mining-2",
                icon_size = 128,
                icon = "__base__/graphics/technology/mining-productivity.png",
                effects =
                {
                  {
                    type = "unlock-recipe",
                    recipe = "explosive-mining-drill-2"
                  }
                },
                prerequisites = {"explosive-mining-1"},
                unit =
                {
                  count = 500,
                  ingredients = data.raw.technology["advanced-material-processing-2"].unit.ingredients,
                  time = 60
                },
                order = "c-k-f"
            },
            {
                type = "technology",
                name = "explosive-mining-3",
                icon_size = 128,
                icon = "__base__/graphics/technology/mining-productivity.png",
                effects =
                {
                  {
                    type = "unlock-recipe",
                    recipe = "explosive-mining-drill-3"
                  }
                },
                prerequisites = {"explosive-mining-2"},
                unit =
                {
                  count = 500,
                  ingredients = data.raw.technology["logistics-3"].unit.ingredients,
                  time = 60
                },
                order = "c-k-f"
            },
            {
                type = "technology",
                name = "explosive-mining-4",
                icon_size = 128,
                icon = "__base__/graphics/technology/mining-productivity.png",
                effects =
                {
                  {
                    type = "unlock-recipe",
                    recipe = "explosive-mining-drill-4"
                  }
                },
                prerequisites = {"explosive-mining-3"},
                unit =
                {
                  count = 750,
                  ingredients = data.raw.technology["rocket-silo"].unit.ingredients,
                  time = 60
                },
                order = "c-k-f"
            }
        }
    )
end