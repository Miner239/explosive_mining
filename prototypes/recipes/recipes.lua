data:extend(
    {
        {
            type = "recipe",
            enabled = "false",
            name = "explosive-mining-drill-1",
            normal =
            {
              energy_required = 30,
              -- normal drill costs 45 iron 7.5 copper, aim for ~10x iron cost
              ingredients = 
              {
                {"electric-mining-drill", 1},
                {"burner-inserter", 30}, -- 3 iron
                {"engine-unit", 30} -- 9 iron
              },
              result = "explosive-mining-drill-1"
            },
            expensive =
            {
              energy_required = 30,
              ingredients =
              {
                {"electric-mining-drill", 2},
                {"burner-inserter", 80},
                {"engine-unit", 60}
              },
              result = "explosive-mining-drill-1"
            }
        }
    }
)

if settings.startup["explosive-mining-enable-tiers"].value == true then
    data:extend(
        {
            {
                type = "recipe",
                enabled = "false",
                name = "explosive-mining-drill-2",
                normal =
                {
                  energy_required = 60,
                  ingredients = 
                  {
                    {"explosive-mining-drill-1", 1},
                    {"iron-stick", 50},
                    {"transport-belt", 30}, 
                    {"pump", 30} 
                  },
                  result = "explosive-mining-drill-2"
                },
                expensive =
                {
                  energy_required = 90,
                  ingredients =
                  {
                    {"explosive-mining-drill-1", 2},
                    {"iron-stick", 100},
                    {"transport-belt", 80}, 
                    {"pump", 60} 
                  },
                  result = "explosive-mining-drill-2"
                }
            },
            {
                type = "recipe",
                enabled = "false",
                category = "crafting-with-fluid",
                name = "explosive-mining-drill-3",
                normal =
                {
                  energy_required = 120,
                  ingredients = 
                  {
                    {"explosive-mining-drill-2", 1},
                    {"steel-plate", 50},
                    {"fast-transport-belt", 30},
                    {"steam-engine", 30},
                    {type="fluid", name="water", amount=4500} 
                  },
                  result = "explosive-mining-drill-3"
                },
                expensive =
                {
                  energy_required = 180,
                  ingredients =
                  {
                    {"explosive-mining-drill-2", 2},
                    {"steel-plate", 100},
                    {"fast-transport-belt", 80},
                    {"steam-engine", 60},
                    {type="fluid", name="water", amount=10000} 
                  },
                  result = "explosive-mining-drill-3"
                }
            },
            {
                type = "recipe",
                enabled = "false",
                category = "crafting-with-fluid",
                name = "explosive-mining-drill-4",
                normal =
                {
                  energy_required = 180,
                  ingredients = 
                  {
                    {"explosive-mining-drill-3", 1},
                    {"low-density-structure", 50}, 
                    {"express-transport-belt", 30}, 
                    {"steam-turbine", 30},
                    {type="fluid", name="lubricant", amount=9000} 
                  },
                  result = "explosive-mining-drill-4"
                },
                expensive =
                {
                  energy_required = 360,
                  ingredients =
                  {
                    {"explosive-mining-drill-3", 2},
                    {"low-density-structure", 100},
                    {"express-transport-belt", 80},
                    {"steam-turbine", 60},
                    {type="fluid", name="lubricant", amount=18000} 
                  },
                  result = "explosive-mining-drill-4"
                }
            }
        }    
    )
end