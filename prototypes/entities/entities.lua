pollution_per_ore = {25,24,23,22} -- electric = 23 boiler, 20 raw
explosive_per_ore = {1/12,1/15,1/18,1/24} -- electric = 90kW = 9/400 coal ~~ 18/400 explosive ~~ 1/20
second_per_ore = {1/18,1/45,1/90,1/240} -- electric = 2
search_radius = {3.49,3.49,4.49,4.49} --electric = 2.49 ~~ 5x5  

function create_miner(tier)
    local newminer = table.deepcopy(data.raw["mining-drill"]["electric-mining-drill"])

    newminer.name = "explosive-mining-drill-"..tier
    newminer.minable.result = newminer.name
    newminer.energy_source =
        {
          type = "burner",
          fuel_category = "explosive",
          effectivity = 1,
          fuel_inventory_size = 2,
          emissions_per_second_per_watt = pollution_per_ore[tier]/explosive_per_ore[tier]/energy_per_explosive/1000000,
          smoke =
          {
            {
              name = "smoke",
              deviation = {0.4, 0.4},
              frequency = 6
            }
          }
        }
    newminer.energy_usage = (energy_per_explosive*explosive_per_ore[tier]/second_per_ore[tier]).."MW" --1.5|3|6|12 explosive/s
    newminer.mining_speed = 1/second_per_ore[tier]
    newminer.resource_searching_radius = search_radius[tier]
    newminer.module_specification = nil -- no modules
    newminer.allowed_effects = {}
    
    data:extend({newminer})
end

create_miner(1)

if settings.startup["explosive-mining-enable-tiers"].value == true then
    for i=2,4 do
        create_miner(i)
    end
end
